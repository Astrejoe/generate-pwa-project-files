# Generate PWA Project Files
Generates some standard files in the process of creating a PWA application. These files include public/manifest.json, public/sitemap.xml, public/robot.xml.

You must create a project-config.js file of type ProjectConfig (see index.ts)
`
