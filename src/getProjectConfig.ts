import * as fs from "fs";
import * as path from "path";
import {ProjectConfig} from "./index";

export default async function getProjectConfig(): Promise<ProjectConfig> {
    const pathToProjectConfig = path.join(process.cwd(), "project-config.js")
    if( !fs.existsSync(pathToProjectConfig) ) {
        throw "ERROR: No project-config.js file found."
    }
    return (await import(pathToProjectConfig)).default
}
