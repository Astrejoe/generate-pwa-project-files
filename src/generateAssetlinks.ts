import * as fs from "fs"
import * as path from "path"
import getProjectConfig from "./getProjectConfig";
import {ProjectConfig} from "./index";

export default async function generateAssetlinks(inputProjectConfig: ProjectConfig | null = null) {
    try {
        const projectConfig: ProjectConfig = inputProjectConfig || await getProjectConfig()
        const assetlinksObj = []
        assetlinksObj.push({
            "relation": ["delegate_permission/common.query_webapk"],
            "target": {
                "namespace": "web",
                "site": `${projectConfig.url}/manifest.json`
            }
        })
        if (projectConfig.play?.sha256_cert_fingerprint && projectConfig.play?.id) {
            assetlinksObj.push({
                "relation": [
                    "delegate_permission/common.handle_all_urls"
                ],
                "target": {
                    "namespace": "android_app",
                    "package_name": projectConfig.play?.id,
                    "sha256_cert_fingerprints": [
                        projectConfig.play?.sha256_cert_fingerprint
                    ]
                }
            })
        }
        const wellKnownDir = path.join(process.cwd(), "public", ".well-known")
        const pathToAssetlinks = path.join(wellKnownDir, "assetlinks.json")
        if (!fs.existsSync(wellKnownDir)) {
            fs.mkdirSync(wellKnownDir);
        }
        fs.writeFile(
            pathToAssetlinks,
            JSON.stringify(assetlinksObj, null, 4),
            function (err) {
                if (err) {
                    return console.error(err);
                }
                console.log(`${pathToAssetlinks} generated successfully!`);
            });
    } catch (e) {
        throw e
    }
}
