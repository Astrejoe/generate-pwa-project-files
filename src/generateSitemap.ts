import * as fs from "fs";
import * as path from "path";
import getProjectConfig from "./getProjectConfig";
import {ProjectConfig} from "./index";

const getAllFiles = function(dirPath: string, recursivePathExtension: string = "", arrayOfFiles: string[] = []) {
    const files = fs.readdirSync(path.join(dirPath, recursivePathExtension))

    files.forEach(function(file) {
        if (fs.statSync(path.join(dirPath, recursivePathExtension, file)).isDirectory()) {
            arrayOfFiles = getAllFiles(dirPath, path.join(recursivePathExtension, file), arrayOfFiles)
        } else {
            arrayOfFiles.push(path.join(recursivePathExtension, file))
        }
    })

    return arrayOfFiles
}

export default async function generateSitemap(inputProjectConfig: ProjectConfig | null = null) {
    try {
        const projectConfig: ProjectConfig = inputProjectConfig || await getProjectConfig()
        const pathToPagesDirectory = path.join(process.cwd(), "pages")
        const files = getAllFiles(pathToPagesDirectory)
        const pageNames = files.map(fileName => {
            const pageName = fileName.substring(0, fileName.indexOf("."))
            return pageName === "index" ? "" : pageName
        })
        const filteredPageNames = pageNames.filter(pageName => pageName !== "_app" && pageName !== "_document")
        const date_ob = new Date();
        const day = ("0" + date_ob.getDate()).slice(-2);
        const month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
        const year = date_ob.getFullYear();
        const date = year + "-" + month + "-" + day
        const sitemapEntries = filteredPageNames.map(pageName => {
            const url = `${projectConfig.url}${pageName === "" ? "" : "/"}${pageName}`
            return `<url><loc>${url}</loc><changefreq>daily</changefreq><priority>${pageName === "" ? "1.0" : "0.5"}</priority><lastmod>${date}</lastmod></url>`
        })
        const pathToSitemap = path.join(process.cwd(), "public", "sitemap.xml")
        const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:mobile="http://www.google.com/schemas/sitemap-mobile/1.0" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">
${sitemapEntries.join("\r\n")}
</urlset>`
        fs.writeFile(
            pathToSitemap,
            sitemap,
            function (err) {
                if (err) {
                    return console.error(err);
                }
                console.log(`${pathToSitemap} generated successfully!`);
            });
    } catch (e) {
        throw e
    }
}
