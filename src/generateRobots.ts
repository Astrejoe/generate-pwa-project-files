import * as fs from "fs"
import * as path from "path"
import getProjectConfig from "./getProjectConfig";
import {ProjectConfig} from "./index";

export default async function generateRobots(inputProjectConfig: ProjectConfig | null = null) {
    try {
        const projectConfig: ProjectConfig = inputProjectConfig || await getProjectConfig()
        const pathToRobots = path.join(process.cwd(), "public", "robots.txt")
        const robots = `# *
User-agent: *
Allow: /

# Host
Host: ${projectConfig.url}

# Sitemaps
Sitemap: ${projectConfig.url}/sitemap.xml
`
        fs.writeFile(
            pathToRobots,
            robots,
            function (err) {
                if (err) {
                    return console.error(err);
                }
                console.log(`${pathToRobots} generated successfully!`);
            });
    } catch (e) {
        throw e
    }
}
