#!/usr/bin/env node
import getProjectConfig from "./getProjectConfig";
import generateSitemap from "./generateSitemap";
import generateManifest from "./generateManifest";
import generateRobots from "./generateRobots";
import generateAssetlinks from "./generateAssetlinks";
import generateWindowsAppWebLink from "./generateWindowsAppWebLink";

export type DarkLightColor = { "light": string, "dark": string }
export type Color =  DarkLightColor | string
export type application = {
    url?: string,
    id?: string,
}
export type ProjectConfig = {
    "name": string,
    "short_name": string,
    "title": string,
    "description": string,
    "keywords"?: string,
    "author": string,
    "theme_color": Color,
    "secondary_theme_color": Color,
    "background_color": Color,
    "display"?: "fullscreen" | "standalone" | "minimal-ui" | "browser",
    "orientation"?: "any" | "natural" | "landscape" | "landscape-primary" | "landscape-secondary" | "portrait" | "portrait-primary" | "portrait-secondary",
    "categories"?: string[],
    "url": string,
    "iarc_rating_id"?: string,
    "prefer_related_applications"?: boolean,
    "play"?: application & {
        "sha256_cert_fingerprint"?: string
    },
    "windows"?: application & {
        "package_family_name"?: string
    }
}

getProjectConfig().then((projectConfig: ProjectConfig) => {
    Promise.all([generateManifest(projectConfig), generateSitemap(projectConfig), generateRobots(projectConfig), generateAssetlinks(projectConfig), generateWindowsAppWebLink(projectConfig)])
        .catch(e => console.error(e))
})
