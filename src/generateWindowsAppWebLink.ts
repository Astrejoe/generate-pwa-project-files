import * as fs from "fs"
import * as path from "path"
import getProjectConfig from "./getProjectConfig";
import {ProjectConfig} from "./index";

export default async function generateWindowsAppWebLink(inputProjectConfig: ProjectConfig | null = null) {
    try {
        const projectConfig: ProjectConfig = inputProjectConfig || await getProjectConfig()
        if (projectConfig.windows?.package_family_name) {
            const webLinkObj = [{
                "packageFamilyName": projectConfig.windows?.package_family_name,
                "paths": [ "*" ]
            }]
            const wellKnownDir = path.join(process.cwd(), "public", ".well-known")
            const pathToWebLinkObj = path.join(wellKnownDir, "windows-app-web-link")
            if (!fs.existsSync(wellKnownDir)) {
                fs.mkdirSync(wellKnownDir);
            }
            fs.writeFile(
                pathToWebLinkObj,
                JSON.stringify(webLinkObj, null, 4),
                function (err) {
                    if (err) {
                        return console.error(err);
                    }
                    console.log(`${pathToWebLinkObj} generated successfully!`);
                });
        } else {
            console.log("Skipped windows app web link generation because project config does not contain necessary info.")
        }
    } catch (e) {
        throw e
    }
}
