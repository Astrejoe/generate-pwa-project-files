import * as fs from "fs";
import * as path from "path";
import getProjectConfig from "./getProjectConfig";
import sizeOf from "image-size";
import {DarkLightColor, ProjectConfig} from "./index";

type screenshot = {
    src: string,
    type: string,
    sizes: string,
}

export default async function generateManifest(inputProjectConfig: ProjectConfig | null = null) {
    try {
        const projectConfig: ProjectConfig = inputProjectConfig || await getProjectConfig()
        const relatedApplications = []
        relatedApplications.push({
            "platform": "webapp",
            "url": `${projectConfig.url}/manifest.json`
        })
        if (projectConfig.play) {
            relatedApplications.push({
                "platform": "play",
                "url": projectConfig.play?.url || `https://play.google.com/store/apps/details?id=${projectConfig.play?.id}`,
                "id": projectConfig.play?.id
            })
        }
        if (projectConfig.windows) {
            relatedApplications.push({
                "platform": "windows",
                "url": projectConfig.windows?.url || `https://www.microsoft.com/store/apps/${projectConfig.windows?.id}`,
                "id": projectConfig.windows?.id
            })
        }
        let screenshots: screenshot[] = []
        try {
            screenshots = fs.readdirSync(path.join(process.cwd(), "public", "screenshots"))
                .map(fileName => {
                    const dimensions = sizeOf(path.join(process.cwd(), "public", "screenshots", fileName))
                    return ({
                        "src": `/screenshots/${fileName}`,
                        "type": "image/png",
                        "sizes": `${dimensions.width}x${dimensions.height}`
                    })
                })
        } catch {
            screenshots = []
        }
        const manifestObj = {
            "name": projectConfig.name,
            "short_name": projectConfig.short_name,
            "description": projectConfig.description,
            "author": projectConfig.author,
            "theme_color": (projectConfig.theme_color as DarkLightColor).light || projectConfig.theme_color,
            "background_color": (projectConfig.background_color as DarkLightColor).light || projectConfig.background_color,
            "display": projectConfig.display || "standalone",
            "orientation": projectConfig.orientation || "any",
            "scope": "/",
            "start_url": "/",
            "categories": projectConfig.categories || [],
            "iarc_rating_id": projectConfig.iarc_rating_id,
            "prefer_related_applications": projectConfig.prefer_related_applications || true,
            "related_applications": relatedApplications,
            "icons": [
                {
                    "src": "/icons/icon.svg",
                    "sizes": "512x512",
                    "type": "image/svg+xml",
                    "purpose": "any"
                },
                {
                    "src": "/icons/icon-192.png",
                    "sizes": "192x192",
                    "type": "image/png",
                    "purpose": "any"
                },
                {
                    "src": "/icons/icon-256.png",
                    "sizes": "256x256",
                    "type": "image/png",
                    "purpose": "any"
                },
                {
                    "src": "/icons/icon-384.png",
                    "sizes": "384x384",
                    "type": "image/png",
                    "purpose": "any"
                },
                {
                    "src": "/icons/icon-512.png",
                    "sizes": "512x512",
                    "type": "image/png",
                    "purpose": "any"
                },
                {
                    "src": "/icons/mask-icon.svg",
                    "sizes": "512x512",
                    "type": "image/svg+xml",
                    "purpose": "maskable"
                },
                {
                    "src": "/icons/mask-icon-192.png",
                    "sizes": "192x192",
                    "type": "image/png",
                    "purpose": "maskable"
                },
                {
                    "src": "/icons/mask-icon-256.png",
                    "sizes": "256x256",
                    "type": "image/png",
                    "purpose": "maskable"
                },
                {
                    "src": "/icons/mask-icon-384.png",
                    "sizes": "384x384",
                    "type": "image/png",
                    "purpose": "maskable"
                },
                {
                    "src": "/icons/mask-icon-512.png",
                    "sizes": "512x512",
                    "type": "image/png",
                    "purpose": "maskable"
                },
                {
                    "src": "/icons/mono-icon.svg",
                    "sizes": "512x512",
                    "type": "image/svg+xml",
                    "purpose": "monochrome"
                },
                {
                    "src": "/icons/mono-icon-48.png",
                    "sizes": "48x48",
                    "type": "image/png",
                    "purpose": "monochrome"
                },
                {
                    "src": "/icons/mono-icon-512.png",
                    "sizes": "512x512",
                    "type": "image/png",
                    "purpose": "monochrome"
                },
            ],
            "screenshots": screenshots
        }
        const pathToManifest = path.join(process.cwd(), "public", "manifest.json")
        fs.writeFile(
            pathToManifest,
            JSON.stringify(manifestObj, null, 4),
            function (err) {
                if (err) {
                    return console.error(err);
                }
                console.log(`${pathToManifest} generated successfully!`);
            });
    } catch (e) {
        throw e
    }
}
